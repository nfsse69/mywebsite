import { Routes, Route, Link, BrowserRouter } from 'react-router-dom'
import Game from './App';
import Layout from './Layout'
import Home from './Home'
import NoPage from './NoPage'
import Auth from './Auth'
import API from './API'

function MainApp() {
   return (
      <div className='App'>
         <BrowserRouter>
            <Routes>
               <Route path="/" element={<Layout />}>
                  <Route index element={<Home />} />
                  <Route path='Game' element={<Game/>} />
                  <Route path='sign-in' element={<Auth/>} />
                  <Route path='API' element={<API/>} />
                  <Route path="*" element={<NoPage />} />
               </Route>
            </Routes>
         </BrowserRouter>
      </div>
   );
}

export default MainApp;